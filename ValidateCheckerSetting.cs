﻿namespace CheckRecourse
{
    public struct ValidateCheckerSetting
    {
        public ValidateCheckerType type;
        public string connectionString;

        /// <summary>
        /// Constructor for ValidateCheckerSetting
        /// </summary>
        /// <param name="connectionString">Connection string for checking it's validation. For example https://ya.ru/</param>
        /// <param name="validateCheckerType">Type of connection</param>

        public ValidateCheckerSetting(string connectionString, ValidateCheckerType validateCheckerType)
        {
            this.connectionString = connectionString;
            type = validateCheckerType;
        }
    }
}
