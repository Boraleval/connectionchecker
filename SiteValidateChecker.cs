﻿using System;
using System.Collections.Generic;
using System.Net;

namespace CheckRecourse
{
    //Class for checking connection to site
    class SiteValidateChecker : ValidateCheckerBase
    {
        public SiteValidateChecker(ValidateCheckerSetting validateCheckerSetting, List<ValidateCheckerBase> validators) : base(validateCheckerSetting, validators) { }
        override protected bool Check()
        {
            try
            {
                Uri uri = new Uri(ValidateCheckerSetting.connectionString);
                try
                {

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                }
                catch (WebException ex)
                {
                    ErrorMessage = string.Format("Error: {0}", ex.Message);
                    IsValid = false;
                    return false;
                }

            }
            catch (UriFormatException ex)
            {
                ErrorMessage = string.Format("Error: {0}", ex.Message);
                IsValid = false;
                return false;
            }
                
            IsValid = true;
            return true;
        }
    }
}
