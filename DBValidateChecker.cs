﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace CheckRecourse
{
    //Class for checking connection to database 
    public class DBValidateChecker : ValidateCheckerBase
    {
        public DBValidateChecker(ValidateCheckerSetting validateCheckerSetting, List<ValidateCheckerBase> validators) : base(validateCheckerSetting, validators) { }

        override protected bool Check()
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ValidateCheckerSetting.connectionString);
                sqlConnection.Open();
            }
            catch (SqlException ex)
            {
                ErrorMessage = string.Format("Error {0}", ex.Message);
                IsValid = false;
                return false;
            }
            IsValid = true;
            return true;
        }
    }
}
