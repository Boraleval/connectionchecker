﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Newtonsoft.Json;
using System.IO;

namespace CheckRecourse
{
    class Program
    {
        private static List<ValidateCheckerBase> checkers = new List<ValidateCheckerBase>();
        private const string JSON_EXTENSION = "JSON";
        private const string SETTINGS_FILE_NAME = "ValidateCheckerSettings";
        private const string SAVE_FILE_NAME = "ValidateCheckerResults";
        private const string LOG_FILE_NAME = "ValidateCheckerLog";

        private static string defaultConnectionSiteString = @"https://ya.ru/";
        private static string defaultConnectionDBString = @"Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;";

        private static string fromAddressName = "serviceAvailable@yandex.ru";
        private static string defaultToAddressName = "boraleval@gmail.com";

        private static StreamWriter streamWriter;
        private static string settingsFileName = string.Empty;
        private static string saveFileName = string.Empty;
        private static string logFileName = string.Empty;

        private static ApplicationSettings applicationSettings = new ApplicationSettings();

        private class ApplicationSettings
        {
            public ValidateCheckerSetting[] checkerSettings = new ValidateCheckerSetting[2]
            {
                new ValidateCheckerSetting(defaultConnectionDBString, ValidateCheckerType.Database),
                new ValidateCheckerSetting(defaultConnectionSiteString, ValidateCheckerType.Web)
            };
            public string[] emailAddresses = new string[1] { defaultToAddressName };
        }

        private static void Main(string[] args)
        {
            try
            {
                //Tset for start with args
                //args = new string[1] { "GetLastResult" };
                saveFileName = Path.ChangeExtension(SAVE_FILE_NAME, JSON_EXTENSION);

                if (IsReadingLastCheckingParam(args))
                {
                    ReadingLastCheckingResult();
                }
                else
                {
                    string result = Checking();

                    File.WriteAllText(saveFileName, result);

                    SendEmailMessage(result, saveFileName);

                    streamWriter.WriteLine(result);
                    streamWriter.Close();
                }

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private static bool IsReadingLastCheckingParam(string[] args)
        {

            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "GetLastResult")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Read and print last file with results of checking recources
        /// </summary>
        private static void ReadingLastCheckingResult()
        {
            try
            {
                Console.WriteLine("Last checking result");
                string jsonString = File.ReadAllText(saveFileName);
                ValidateCheckerBase[] lastResult = JsonConvert.DeserializeObject<ValidateCheckerBase[]>(jsonString);
                for (int i = 0; i < lastResult.Length; i++)
                {
                    lastResult[i].PrintResult();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// checking all recources from application settings file
        /// </summary>
        private static string Checking()
        {
            settingsFileName = Path.ChangeExtension(SETTINGS_FILE_NAME, JSON_EXTENSION);
            logFileName = Path.ChangeExtension(LOG_FILE_NAME, JSON_EXTENSION);


            if (File.Exists(settingsFileName))
            {
                string settingsJson = File.ReadAllText(settingsFileName);
                applicationSettings = JsonConvert.DeserializeObject<ApplicationSettings>(settingsJson);
            }

            GetCheckers();

            if (!File.Exists(logFileName))
            {
                File.Create(logFileName);
            }
            streamWriter = File.AppendText(logFileName);

            foreach (ValidateCheckerBase validateChecker in checkers)
            {
                validateChecker.CheckIsValid();
                validateChecker.PrintResult();
            }

            string jsonString = JsonConvert.SerializeObject(checkers);

            return jsonString;
        }

        /// <summary>
        /// Collect all checkers from application settings file
        /// </summary>
        private static void GetCheckers()
        {
            if (applicationSettings.checkerSettings.Length > 0)
            {
                foreach (ValidateCheckerSetting setting in applicationSettings.checkerSettings)
                {
                    if (setting.type == ValidateCheckerType.Database)
                    {
                        new DBValidateChecker(setting, checkers);
                    }
                    if (setting.type == ValidateCheckerType.Web)
                    {
                        new SiteValidateChecker(setting, checkers);
                    }
                }
            }
        }

        /// <summary>
        /// Send email to addresses from application settings file
        /// </summary>
        /// <param name="message">Email body text</param>
        /// <param name="fileName">File for attach</param>

        private static void SendEmailMessage(string message, string fileName)
        {
            MailMessage mailMessage = new MailMessage();

            MailAddress fromMailAddress;

            bool isValidFromMail = IsValidEmail(fromAddressName, out fromMailAddress);

            if (!isValidFromMail)
                return;

            AddAddresses(mailMessage);

            mailMessage.From = fromMailAddress;

            mailMessage.Subject = "Checking site " + DateTime.Now;
            mailMessage.Body = "Result of connection \n" + message;
            Attachment attachment = new Attachment(fileName);
            mailMessage.Attachments.Add(attachment);


            SmtpClient smtpClient = new SmtpClient("smtp.yandex.ru");
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new NetworkCredential(fromAddressName, "Z1c2b3m4.5");
            try
            {
                smtpClient.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
                string messageLog = string.Format("Невозможно отправить письмо с результатми подключения. Проверьте правильно ли указаны параметры smtp подключения.");
                messageLog += string.Format("Error {0}.", ex.StatusCode);

                Console.WriteLine(messageLog);
                streamWriter.WriteLine(DateTime.Now + " " + messageLog);
            }
        }

        /// <summary>
        /// Collect and add to mail addresses from application settings file
        /// </summary>
        private static void AddAddresses(MailMessage mailMessage)
        {
            if (applicationSettings.emailAddresses.Length > 0)
            {
                for (int i = 0; i < applicationSettings.emailAddresses.Length; i++)
                {
                    string toAddressName = applicationSettings.emailAddresses[i];
                    MailAddress toMailAddress;
                    bool isValidEmail = IsValidEmail(toAddressName, out toMailAddress);
                    if (isValidEmail)
                    {
                        mailMessage.To.Add(toMailAddress);
                    }
                }
            }
        }

        private static bool IsValidEmail(string mail, out MailAddress mailAddress)
        {
            try
            {
                mailAddress = new MailAddress(mail);
                return mailAddress.Address == mail;
            }
            catch
            {
                mailAddress = null;
                string messageLog = string.Format("Некорректный адрес почты {0}.", mail);
                Console.WriteLine(messageLog);
                streamWriter.WriteLine(DateTime.Now + " " + messageLog);
                return false;
            }
        }
    }
}
