﻿using System;
using System.Collections.Generic;

namespace CheckRecourse
{
    //Base class for checking connection
    public class ValidateCheckerBase
    {
        public ValidateCheckerSetting ValidateCheckerSetting { get; set; }
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime CheckingTime { get; set; }

        public ValidateCheckerBase(){ }
        public ValidateCheckerBase(ValidateCheckerSetting validateCheckerSetting, List<ValidateCheckerBase> validators)
        {
            ValidateCheckerSetting = validateCheckerSetting;
            validators.Add(this);
        }

        public void PrintResult()
        {
            string message;
            if (IsValid)
            { message = string.Format("Соединение {0} доступно. Время {1}.", ValidateCheckerSetting.connectionString, CheckingTime); }
            else
            { message = string.Format("Соединение {0} недоступно. {1} Время {2}.", ValidateCheckerSetting.connectionString, ErrorMessage, CheckingTime); }
            Console.WriteLine(message);
        }

        /// <summary>
        /// Check is resource valid. Set resource in ValidateCheckerSetting.
        /// </summary>

        public void CheckIsValid()
        {
            Check();
            CheckingTime = DateTime.Now;
        }

        virtual protected bool Check() { return false; }

    }
}
